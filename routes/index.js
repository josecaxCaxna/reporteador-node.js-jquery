var express = require('express');
var router = express.Router();

const json2xls = require('json2xls');
const fs = require('fs');
const excelJson = require('convert-excel-to-json');
const sql = require('mssql');

const result = excelJson({
   sourceFile: 'uuids.xlsx',
   header:{
       rows: 1
   }
});
console.log(result);

/**
 * @description Configuracion de conexion a base de datos
 */
const config = {
   user: process.env.DB_USER || 'jflores.c',
   password: process.env.DB_PASSWORD || 'Tx578rm41l',
   server: process.env.DB_SERVER || '40.70.190.149',
   // server: process.env.DB_SERVER || '31.1.10.8',
   database: process.env.DB_DATABASE || 'V5AgpNomina',
   encrypt: false,
   // connectionTimeout : 1500000,
   // requestTimeout: 1500000
}

/**
 * @description Ruta para cargar la vista principal
 */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Reporteador' });
});

/**
 * @description Ruta para la vista de modificacion de fechas
 */
router.get('/modDate', function(req, res, next) {
   res.status(200).render('modDate', { title: 'Moficiación de fechas', data:'' });
});

/**
 * @description Ruta para la viste de modificacion de empleados
 */
router.get('/modColab', function(req, res, next) {
   res.status(200).render('modColab', { title: 'Moficiación de colaboradores', data:'' });
});

/**
 * @description Ruta para la viste de periodos activos inactivos
 */
router.get('/inactPeriod', function(req, res, next) {
   res.status(200).render('modPeriod', { title: 'Moficiación de periodos', data:'' });
});

/**
 * @description Ruta para la vista de reporte de ISN
 */
 router.get('/reportIsn', function(req, res, next) {
   res.status(200).render('reportIsn', { title: 'Generar reporte', data:'' });
});

/**
 * @description Ruta para consultar a los empleados con sus filtros
 */
router.post('/modColabPost', function(req, res, next) {   
   let razonsocialE = req.body.razonsocialE;
   let nomAsigE = req.body.nomAsigE;
   let nomProcE = req.body.nomProcE;
   let periodoE = req.body.periodoE;
   let anioPeriodoE = req.body.anioPeriodoE;
   let numEmpE = req.body.numEmpE;
   let radioAll = req.body.radioAll;
   const request = new sql.Request();
   let query;
   if (razonsocialE && nomAsigE && nomProcE && periodoE && anioPeriodoE && numEmpE && radioAll === 'on') {
      spliterString = numEmpE.split(',');
      var quotedSepar = "'" + spliterString.join("','") + "'";
      query = `SELECT * FROM NOMEMPLEADONOMINAACUMULADO WHERE IDTIPONOMINAASIG IN('${nomAsigE}') AND IDTIPONOMINAPROC IN('${nomProcE}') AND IDRAZONSOCIAL IN('${razonsocialE}') AND ANIO IN('${anioPeriodoE}') AND PERIODO IN('${periodoE}') AND IDEMPLEADO IN (${quotedSepar});`;
   }else if (razonsocialE && nomAsigE && nomProcE && periodoE && anioPeriodoE && numEmpE && radioAll === 'off') {
      spliterString = numEmpE.split(',');
      var quotedSepar = "'" + spliterString.join("','") + "'";
      query = `SELECT * FROM NOMEMPLEADONOMINAACUMULADO WHERE IDTIPONOMINAASIG IN('${nomAsigE}') AND IDTIPONOMINAPROC IN('${nomProcE}') AND IDRAZONSOCIAL IN('${razonsocialE}') AND ANIO IN('${anioPeriodoE}') AND PERIODO IN('${periodoE}') AND IDEMPLEADO NOT IN (${quotedSepar});`;
   }
   if (razonsocialE && nomAsigE && nomProcE && periodoE && anioPeriodoE && !numEmpE && radioAll === 'on') {
      query = `SELECT * FROM NOMEMPLEADONOMINAACUMULADO WHERE IDTIPONOMINAASIG IN('${nomAsigE}') AND IDTIPONOMINAPROC IN('${nomProcE}') AND IDRAZONSOCIAL IN('${razonsocialE}') AND ANIO IN('${anioPeriodoE}') AND PERIODO IN('${periodoE}');`;
   }
   request.query(query, (err, result) => {
      if (err) res.status(500).send(err);
      if (result) {
         datas = result['recordsets'][0];
         res.status(200).json({data: datas});
      }
   });
});

/**
 * @description Ruta para actualizar a los colaboradores, ponerlos activos e inativos
 */
router.post('/postColabUpdate', function(req, res, next) {   
   let razonsocialE = req.body.razonsocialE;
   let nomAsigE = req.body.nomAsigE;
   let nomProcE = req.body.nomProcE;
   let periodoE = req.body.periodoE;
   let anioPeriodoE = req.body.anioPeriodoE;
   let numEmpE = req.body.numEmpE;
   let radioAll = req.body.radioAll;
   let newIdAsig = req.body.newIdAsig;
   const request = new sql.Request();
   let query;
   if (razonsocialE && nomAsigE && nomProcE && periodoE && anioPeriodoE && numEmpE && radioAll === 'on' && newIdAsig) {
      spliterString = numEmpE.split(',');
      var quotedSepar = "'" + spliterString.join("','") + "'";
      query = `UPDATE NOMEMPLEADONOMINAACUMULADO SET IdTipoNominaAsig= '${newIdAsig}' WHERE IDTIPONOMINAASIG IN('${nomAsigE}') AND IDTIPONOMINAPROC IN('${nomProcE}') AND IDRAZONSOCIAL IN('${razonsocialE}') AND ANIO IN('${anioPeriodoE}') AND PERIODO IN('${periodoE}') AND IDEMPLEADO IN (${quotedSepar});`;
   }else if (razonsocialE && nomAsigE && nomProcE && periodoE && anioPeriodoE && numEmpE && radioAll === 'off' && newIdAsig) {
      spliterString = numEmpE.split(',');
      var quotedSepar = "'" + spliterString.join("','") + "'";
      query = `UPDATE NOMEMPLEADONOMINAACUMULADO SET IDTIPONOMINAASIG = '${newIdAsig}' WHERE IDTIPONOMINAASIG IN('${nomAsigE}') AND IDTIPONOMINAPROC IN('${nomProcE}') AND IDRAZONSOCIAL IN('${razonsocialE}') AND ANIO IN('${anioPeriodoE}') AND PERIODO IN('${periodoE}') AND IDEMPLEADO NOT IN (${quotedSepar});`;
   }
   if (razonsocialE && nomAsigE && nomProcE && periodoE && anioPeriodoE && !numEmpE && radioAll === 'on' && newIdAsig) {
      query = `UPDATE NOMEMPLEADONOMINAACUMULADO SET IDTIPONOMINAASIG = '${newIdAsig}' WHERE IDTIPONOMINAASIG IN('${nomAsigE}') AND IDTIPONOMINAPROC IN('${nomProcE}') AND IDRAZONSOCIAL IN('${razonsocialE}') AND ANIO IN('${anioPeriodoE}') AND PERIODO IN('${periodoE}');`;
   }
   request.query(query, (err, result) => {
      if (err) res.status(500).send(err);
      if (result) {
         datas = result['rowsAffected'][0];
         res.status(200).json({data: datas});
      }
   });
});

/**
 * @description Ruta para obtener la fecha actual en nomcalendario
 */
router.post('/getDate', function(req, res, next) {
   const request = new sql.Request();
   const dataResponse = `SELECT * FROM NOMCALENDARIO WHERE IDRAZONSOCIAL IN('${req.body.razonsocial}') AND IDTIPONOMINA IN('${req.body.nomAsig}') AND ANIOPERIODO IN('${req.body.anioPeriodo}') AND PERIODO IN ('${req.body.periodo}');`;
   let datas;
   request.query(dataResponse, (err, result) => {
      if (err) res.status(500).send(err);
      if (result) {
         datas = result['recordsets'][0];
         res.status(200).json({data: datas});
      }
   });
});

/**
 * @description Ruta para actualizar la fecha de pago en nomcalendario
 */
router.post('/postChangeDate', function(req, res, next) {
   let razonsocial = req.body.razonsocial;
   let nomAsig = req.body.nomAsig;
   let periodo = req.body.periodo;
   let anioPeriodo = req.body.anioPeriodo;
   let fecAct = req.body.fecAct;
   let dateResSplit = fecAct.toString().split("-");
   let dateFull = `${dateResSplit[2]}-${dateResSplit[1]}-${dateResSplit[0]}`;
   const request = new sql.Request();
   const resul = `set dateformat DMY update nomCalendario set FechaPago = '${dateFull}' where IdRazonSocial = ${razonsocial} and Periodo in ('${periodo}') and AnioPeriodo in('${anioPeriodo}') and IdTipoNomina = '${nomAsig}';`;
   request.query(resul, (err, result) => {
      if (err) res.status(500).send(err);
      let dataResponse = [];
      if (result) {
         dataResponse = result['rowsAffected'];
         dataResponse.push(dateFull);
         res.status(200).json(dataResponse);
      }
   });
});

/**
 * @description Ruta para generar el reporte de activos y bajas
 */
router.get('/report', async (req, res) => {
   // try {
      let dataJson = [];
      const request = new sql.Request();
      request.query(`update nomempleadonomina set idbanco = '999' where idbanco = '' or idbanco = null;`);
      const activosQuery = `select nen.idrazonsocial RAZON, CASE WHEN nen.status = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END STATUS, nen.idempleado #EMPLEADO, p.nombre NOMBRE, p.apellidopat APELLIDO_PATERNO, p.apellidomat APELLIDO_MATERNO,    (p.apellidopat+' '+p.apellidomat+' '+p.nombre) as NOMBRE_COMPLETO,     p.email CORREO_ELECTRONICO,     DOMICILIO = UPPER(p.domicilioresidencia+',Col. '+p.coloniaresidencia+', '+p.idEstadoResidencia+', C.P. '+p.cpresidencia+', '+p.idPaisResidencia),  +char(39)+convert( varchar(11), REPLICATE('0', 11 - LEN(cast(nen.afiliacionimss  as varchar(11))))+convert(varchar(11),cast(nen.afiliacionimss  as varchar(11)))) NSS,  nen.RFC,nen.CURP, p.telefonoMovil TELEFONO,  nen.idregistropatronal REGISTRO_PATRONAL,  pat.NumeroPatronal NUMERO_PATRONAL,  nen.idtiponominaasig #NOM,  ntn.descripcion NOMINA,  convert(varchar(10), P.FECHANAC, 103) FECHA_NACIMIENTO,  convert(varchar(10), nen.fechaalta, 103) FECHA_ALTA,  convert(varchar(10), nen.FechaIngreso , 103) FECHA_INGRESO,  convert(varchar(10), nen.FechaBaja , 103) FECHA_BAJA,  nen.salariodiario SD,  NEN.SalarioDiarioINFONAVIT SBC,  nen.SalarioDiarioIntegrado SDI,  b.Descripcion BANCO,  +char(39)+CONVERT(varchar(50), nen.CuentaBancaria) CUENTA_BANCARIA,  nen.IdVacacion VACACION,  nen.IdAguinaldo AGUINALDO,  lp.Descripcion LUGAR_PAGO,  po.nombre PUESTO  from nomempleadonomina nen, nomTipoNomina ntn, Persona p, nomRegistroPatronal pat,  nomLugarPago lp, nombanco b, v5agpnomina..vposicion po  where ntn.idrazonsocial=nen.idrazonsocial  and ntn.idtiponomina=nen.idtiponominaasig  and nen.IdRazonSocial in  (nen.IdRazonSocial)  and nen.idpersona=p.idpersona  AND nen.Status = 1  and nen.IdRazonSocial = pat.IdRazonSocial  and isnull(nen.IdRegistroPatronal,1) = pat.IdRegistroPatronal  and isnull(nen.IdLugarPago,15) = lp.IdLugarPago  and lp.IdRazonSocial=nen.IdRazonSocial  and nen.idrazonsocial = b.IdRazonSocial and nen.IdBanco = isnull(b.IdBanco,'999') and nen.idPosicion = po.idPosicion order by Razon, #NOM, #EMPLEADO`;
      const inactivosQuery = `select nen.idrazonsocial RAZON,  CASE WHEN nen.status=1 THEN 'ACTIVO' ELSE 'INACTIVO' END STATUS,  nen.idempleado #EMPLEADO,  p.nombre NOMBRE,  p.apellidopat APELLIDO_PATERNO,  p.apellidomat APELLIDO_MATERNO,    (p.apellidopat+' '+p.apellidomat+' '+p.nombre) as NOMBRE_COMPLETO,     p.email CORREO_ELECTRONICO,     DOMICILIO = UPPER(p.domicilioresidencia+',Col. '+p.coloniaresidencia+', '+p.idEstadoResidencia+', C.P. '+p.cpresidencia+', '+p.idPaisResidencia),  +char(39)+convert( varchar(11), REPLICATE('0', 11 - LEN(cast(nen.afiliacionimss  as varchar(11))))+convert(varchar(11),cast(nen.afiliacionimss  as varchar(11)))) NSS,  nen.RFC,nen.CURP, ltrim(rtrim(p.telefonoMovil)) TELEFONO,  rtrim(ltrim(nen.idregistropatronal)) REGISTRO_PATRONAL,  pat.NumeroPatronal NUMERO_PATRONAL,  nen.idtiponominaasig #NOM,  ntn.descripcion NOMINA,  convert(varchar(10), P.FECHANAC, 103) FECHA_NACIMIENTO,  convert(varchar(10), nen.fechaalta, 103) FECHA_ALTA,  convert(varchar(10), nen.FechaIngreso , 103) FECHA_INGRESO,  convert(varchar(10), nen.FechaBaja , 103) FECHA_BAJA,  nen.salariodiario SD,  NEN.SalarioDiarioINFONAVIT SBC,  nen.SalarioDiarioIntegrado SDI,  b.Descripcion BANCO,  +char(39)+CONVERT(varchar(50), nen.CuentaBancaria) CUENTA_BANCARIA,  nen.IdVacacion VACACION,  nen.IdAguinaldo AGUINALDO,lp.Descripcion LUGAR_PAGO, po.nombre PUESTO from nomempleadonomina nen, nomTipoNomina ntn, Persona p, nomRegistroPatronal pat,nomLugarPago lp, nombanco b, posicion po where ntn.idrazonsocial=nen.idrazonsocial and ntn.idtiponomina=nen.idtiponominaasig and nen.IdRazonSocial in (nen.IdRazonSocial) and nen.idpersona=p.idpersona AND nen.Status = 0 and nen.IdRazonSocial = pat.IdRazonSocial and isnull(nen.IdRegistroPatronal,1) = pat.IdRegistroPatronal and isnull(nen.IdLugarPago,15) = lp.IdLugarPago and lp.IdRazonSocial=nen.IdRazonSocial and nen.idrazonsocial = b.IdRazonSocial and nen.IdBanco = isnull(b.IdBanco,'999') and nen.idPosicion = po.idPosicion order by Razon, #NOM, #EMPLEADO`;
      request.query(activosQuery, (err, result) => {
         if (err) res.ststus(500).render('error', { title: "Algo fallo.", error: error });
         if (!err) {
            dataJson = result['recordsets'][0];  
            request.query(inactivosQuery, (err, results) => {
               if (err) res.status(500).send(err);
               if (!err) {
                  for (let i = 0; i < results['recordsets'][0].length; i++) {
                     dataJson.push(results['recordsets'][0][i]);
                  }
                  reportFunc(dataJson);
                  var fileName = "reporte.xlsx";
                  res.download(fileName);  
               }
            });
         }
      });      
   // } catch (error) {
   //    console.log(`Error al generar reporte activos/bajas: ${error}`);
   //    res.status(500).send(err);
   // }   
});

/**
 * @description Ruta para consultar los periodos activos o inactivos
 */
 router.post('/getInactPerdiod', function(req, res, next) {   
   let razonsocialI = req.body.razonsocialI;
   let nomAsigI = req.body.nomAsigI;
   let nomProcI = req.body.nomProcI;
   let periodoI = req.body.periodoI;
   let anioPeriodoI = req.body.anioPeriodoI;
   const request = new sql.Request();
   let query = `SELECT * FROM NOMCALENDARIO WHERE IDRAZONSOCIAL IN ('${razonsocialI}') AND PERIODO IN ('${periodoI}') AND IDTIPONOMINA IN('${nomAsigI}') AND ANIOPERIODO IN('${anioPeriodoI}');`;   
   request.query(query, (err, result) => {
      if (err) res.status(500).send(err);
      if (result) {
         let datas = result['recordset'];
         res.status(200).json(datas);
      }
   });
});

/**
 * @description Ruta para obtener la informacion de los periodos, nomMovto y nomMovtoHis
 */
router.post('/getDataMovtoMovtoHis', async function(req, res, next){
   try {
      let pool = await sql.connect(config);
      let razonsocialI = req.body.razonsocialI;
      let nomAsigI = req.body.nomAsigI;
      let nomProcI = req.body.nomProcI;
      let periodoI = req.body.periodoI;
      let anioPeriodoI = req.body.anioPeriodoI;
      movto = await pool.request().query(`select * from nomEmpleadoMovto where IdRazonSocial IN('${razonsocialI}') and Periodo IN(${periodoI}) and IdTipoNominaAsig in('${nomAsigI}') and IdTipoNominaProc in('${nomProcI}') and Anio in('${anioPeriodoI}')`);
      movtohis = await pool.request().query(`select * from nomEmpleadoMovtoHis where IdRazonSocial IN('${razonsocialI}') and Periodo IN(${periodoI}) and IdTipoNominaAsig in('${nomAsigI}') and IdTipoNominaProc in('${nomProcI}') and Anio in('${anioPeriodoI}')`);
      res.status(200).send({movto,movtohis});
   } catch (error) {
      console.log(`Error al obtener los estatus de los periodos: ${error}`);
      res.status(500).send(error);
   }
});

/**
 * @description Ruta para modificar los periodos: abierto / cerrado
 */
 router.post('/updPerActiInacti/:type', async function(req, res, next){
   try {
      let pool = await sql.connect(config);
      const type = req.params.type;
      let razonsocialI = req.body.razonsocialI;
      let nomAsigI = req.body.nomAsigI;
      let nomProcI = req.body.nomProcI;
      let periodoI = req.body.periodoI;
      let anioPeriodoI = req.body.anioPeriodoI;
      if (type === 'open') {
         let verifyPer = await pool.request().query(`select * from nomEmpleadoMovto where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNominaAsig in('${nomAsigI}') and IdTipoNominaProc in ('${nomProcI}') and Anio in('${anioPeriodoI}');`);
         let verifyPerHis = await pool.request().query(`select * from nomEmpleadoMovtoHis where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNominaAsig in('${nomAsigI}') and IdTipoNominaProc in ('${nomProcI}') and Anio in('${anioPeriodoI}');`);
         let verifyCalendarClose = await pool.request().query(`select * from nomCalendario where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNomina in('${nomAsigI}') and AnioPeriodo in('${anioPeriodoI}');`);
         if (verifyPer.rowsAffected == 0 && verifyPerHis.rowsAffected > 0 && verifyCalendarClose.recordset[0]['Status'] === '6') {
            await pool.request().query(`insert into nomEmpleadoMovto select * from nomempleadomovtoHis where IdRazonSocial in('${razonsocialI}') and IdTipoNominaAsig in('${nomAsigI}') and anio in('${anioPeriodoI}') and periodo in('${periodoI}');`);
            await pool.request().query(`delete from nomEmpleadoMovtoHis where IdRazonSocial in('${razonsocialI}') and IdTipoNominaAsig in('${nomAsigI}') and anio in('${anioPeriodoI}') and periodo in('${periodoI}');`);
            await pool.request().query(`update nomCalendario set status=1 where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNomina in('${nomAsigI}') and AnioPeriodo in('${anioPeriodoI}');`);
            res.status(200).send({'message':`El periodo ha sido abierto correctamente. Movto: ${verifyClosePer.rowsAffected}, MovtoHis: ${verifyClosePerHis.rowsAffected}, Calendario status: ${verifyCalendarOpen.recordset[0]['Status']}`,'type':'success'});
         }else{
            res.status(200).send({'message':`El periodo no ha sido alterado. Estatus de datos... Movto: ${verifyPer.rowsAffected}, MovtoHis: ${verifyPerHis.rowsAffected}, Calendario status: ${verifyCalendarClose.recordset[0]['Status']}`,'type':'error'});
         }      
      }
      if (type === 'close') {
         let verifyClosePer = await pool.request().query(`select * from nomEmpleadoMovto where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNominaAsig in('${nomAsigI}') and IdTipoNominaProc in ('${nomProcI}') and Anio in('${anioPeriodoI}');`);
         let verifyClosePerHis = await pool.request().query(`select * from nomEmpleadoMovtoHis where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNominaAsig in('${nomAsigI}') and IdTipoNominaProc in ('${nomProcI}') and Anio in('${anioPeriodoI}');`);
         let verifyCalendarOpen = await pool.request().query(`select * from nomCalendario where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNomina in('${nomAsigI}') and AnioPeriodo in('${anioPeriodoI}');`);
         if (verifyClosePer.rowsAffected > 0 && verifyClosePerHis.rowsAffected == 0 && verifyCalendarOpen.recordset[0]['Status'] === '1') {
            await pool.request().query(`insert into nomEmpleadoMovtoHis select * from nomempleadomovto where IdRazonSocial in('${razonsocialI}') and IdTipoNominaAsig in('${nomAsigI}') and anio in('${anioPeriodoI}') and periodo in('${periodoI}');`);
            await pool.request().query(`delete from nomEmpleadoMovto where IdRazonSocial in('${razonsocialI}') and IdTipoNominaAsig in('${nomAsigI}') and anio in('${anioPeriodoI}') and periodo in('${periodoI}');`);
            await pool.request().query(`update nomCalendario set status = 6 where IdRazonSocial in('${razonsocialI}') and Periodo in('${periodoI}') and IdTipoNomina in('${nomAsigI}') and AnioPeriodo in('${anioPeriodoI}');`);
            res.status(200).send({'message':`El periodo ha sido cerrado correctamente. Movto: ${verifyClosePer.rowsAffected}, MovtoHis: ${verifyClosePerHis.rowsAffected}, Calendario status: ${verifyCalendarOpen.recordset[0]['Status']}`,'type':'success'});
         }else{
            res.status(200).send({'message':`El periodo no ha sido alterado. Estatus de datos... Movto: ${verifyClosePer.rowsAffected}, MovtoHis: ${verifyClosePerHis.rowsAffected}, Calendario status: ${verifyCalendarOpen.recordset[0]['Status']}`,'type':'error'});
         }         
      }
   } catch (error) {
      console.log(`Error al abrir el periodo: ${error}`);
      res.status(500).send(error);
   }
});

/**
 * @description Metodo para realizar la conexion a base de datos
 */
const db = sql.connect(config, error => {
   try {
      
   } catch (error) {
      if (error) {
         console.log('Failed to open a SQL Database connection.', error.stack);
         // process.exit(1);
      }  
   }
});

/**
 * @description Función para crear el reporte de activos y bajas
 * @param { QueryResult } dat 
 */
function reportFunc(dat){
     let data = dat;
     var xls = json2xls(data);
  fs.writeFileSync('reporte.xlsx', xls,'binary', (err) => {
     if (err) {
           console.log("writeFileSync :", err);
        }
     console.log("report file is saved!");
  });
}

/**
 * @description Codigo que sirve para detectar cuando se cierra o se cancela el proceso de ejecucion en la terminal
 */
process.stdin.resume();
function exitHandler(options, exitCode) {
    if (options.cleanup) console.log('clean');
    if (exitCode || exitCode === 0) console.log(exitCode);
    if (options.exit) process.exit();
    db.close();
}
process.on('exit', exitHandler.bind(null,{cleanup:true}));
process.on('SIGINT', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));


module.exports = router;