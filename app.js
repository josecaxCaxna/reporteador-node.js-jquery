/**
   * @description Programa para generar el reporte de activos y bajas.
   * Para ejecutar el programa se realiza con: node index.js
   * @description La libreria para ejecutarlo es: npm install -g pkg
   * Link de la libreria: https://www.npmjs.com/package/pkg
   * Para empaquetar el programa se realiza con: pkg . --targets node10-win-x64
*/
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/public/javascripts'));
app.use('/', indexRouter);

module.exports = app;