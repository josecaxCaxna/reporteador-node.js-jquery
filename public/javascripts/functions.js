/**
 * @description Funcion para actualizar la fecha de pago
 * @returns {message: 'confirmation'}
 */
function refreshDate(){
    var url = "/postChangeDate";
    var data = $('form').serializeArray();
    var verify = verifyForm(data,2);
    if (verify) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success:function(response,status) {
                if(response && status === 'success') {
                    if (response[0] === 1 ) {
                        let dateUpdated = response[1];
                        $('#actuallDate').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>La fecha ha sido actualizada exitosamente. Dato: </strong>${dateUpdated}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    }else{
                        $('#actuallDate').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>!Error no se ha modificado la fecha!!!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    }
                }
            },
            error: function(xhr, error) {
                if (xhr.responseText) {
                    let error = xhr.responseText;
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong> Error: ${error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }else{
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }
            }
        });
    }
}


/**
 * @description Funcion para obtener la fecha actual en modificar fechas, esto para saber cual es la fecha actual.
 * @returns {Date} Retorna a vista el mensaje con la fecha actual
 * @returns {error} Retorna el error en la vista o error de servidor cuando no responde
 */
function getDate(){
    var url = "/getDate";
    var data = $('form').serializeArray();
    var verify = verifyForm(data,1);
    if (verify) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success:function(response,status) {
                if(response && status === 'success') {
                    if (response.data.length > 0) {
                        $('#actuallDate').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <strong>Fecha actual: </strong> ${ new Date(response.data[0].FechaPago).toUTCString() }
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    } else {
                        $('#actuallDate').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>!Error revisa los datos ingresados, no se ha encontrado ningún registro!!!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    }                    
                }
            },
            error: function(xhr, error) {
                if (xhr.responseText) {
                    let error = xhr.responseText;
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong> Error: ${error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }else{
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }                
            }
        });
    }
}

/**
 * @description Funcion para limpiar el formulario: Cambio de fecha.
 */
function clearForm(){
    $( "#razonsocial" ).removeClass( "is-invalid");
    $( "#razonsocial" ).removeClass( "is-valid");
    $( "#nomAsig" ).removeClass( "is-invalid");
    $( "#nomAsig" ).removeClass( "is-valid");
    $( "#nomProc" ).removeClass( "is-invalid");
    $( "#nomProc" ).removeClass( "is-valid");
    $( "#anioPeriodo" ).removeClass( "is-invalid");
    $( "#anioPeriodo" ).removeClass( "is-valid");
    $( "#periodo" ).removeClass( "is-invalid");
    $( "#periodo" ).removeClass( "is-valid");
    $( "#fecAct" ).removeClass( "is-invalid");
    $( "#fecAct" ).removeClass( "is-valid");
    $('form')[0].reset();
}


/**
     * @description Funcion que evalua sí el formulario de cambio de fecha ha sido llenado.
     * @param { datos_formulario } data 
     * @returns true/false 
 */
function verifyForm(data,paramType){
    if (paramType === 1) {
        var razonsocial = data[0].value;
        var nomAsig = data[1].value;
        var anioPeriodo = data[2].value;
        var periodo = data[3].value;
        if (!razonsocial) {
            $( "#razonsocial" ).addClass( "is-invalid");    
        }else{
            $( "#razonsocial" ).removeClass( "is-invalid");
            $( "#razonsocial" ).addClass( "is-valid");
        }
        if (!nomAsig) {
            $( "#nomAsig" ).addClass( "is-invalid");    
        }else{
            $( "#nomAsig" ).removeClass( "is-invalid");
            $( "#nomAsig" ).addClass( "is-valid");
        }
        if (!anioPeriodo) {
            $( "#anioPeriodo" ).addClass( "is-invalid");    
        }else{
            $( "#anioPeriodo" ).removeClass( "is-invalid");
            $( "#anioPeriodo" ).addClass( "is-valid");
        }
        if (!periodo) {
            $( "#periodo" ).addClass( "is-invalid");    
        }else{
            $( "#periodo" ).removeClass( "is-invalid");
            $( "#periodo" ).addClass( "is-valid");
        }
        if (razonsocial && nomAsig && anioPeriodo && periodo) {
            return true;
        }else{
            return false;
        }
    }else if(paramType === 2){
        var razonsocial = data[0].value;
        var nomAsig = data[1].value;
        var anioPeriodo = data[2].value;
        var periodo = data[3].value;
        var fecAct = data[4].value;
        if (!razonsocial) {
            $( "#razonsocial" ).addClass( "is-invalid");    
        }else{
            $( "#razonsocial" ).removeClass( "is-invalid");
            $( "#razonsocial" ).addClass( "is-valid");
        }
        if (!nomAsig) {
            $( "#nomAsig" ).addClass( "is-invalid");    
        }else{
            $( "#nomAsig" ).removeClass( "is-invalid");
            $( "#nomAsig" ).addClass( "is-valid");
        }
        if (!anioPeriodo) {
            $( "#anioPeriodo" ).addClass( "is-invalid");    
        }else{
            $( "#anioPeriodo" ).removeClass( "is-invalid");
            $( "#anioPeriodo" ).addClass( "is-valid");
        }
        if (!periodo) {
            $( "#periodo" ).addClass( "is-invalid");    
        }else{
            $( "#periodo" ).removeClass( "is-invalid");
            $( "#periodo" ).addClass( "is-valid");
        }
        if (!fecAct) {
            $( "#fecAct" ).addClass( "is-invalid");    
        }else{
            $( "#fecAct" ).removeClass( "is-invalid");
            $( "#fecAct" ).addClass( "is-valid");
        }        
        if (razonsocial && nomAsig && anioPeriodo && periodo && fecAct) {
            return true;
        }else{
            return false;
        }
    }
}

/**
 * @description Funcion para consultar los colaboradores que se encuentran activos e inactivos
 * @param { form } data
 */
function colabsData(){
    let url = "/modColabPost";
    let data = [];
    data = $('form').serializeArray();
    let verifyCheck = $('#checkColabs').is(":checked");
    let verify;
    if (verifyCheck) {
        verify = verifyColabsForm(data,1,'consult');
    }else{
        verify = verifyColabsForm(data,2,'consult');
    }
    if (verify) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success:function(response,status) {
                $('#tabColabs').append(`
                <div class="col-sm-12.col-md-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="table-responsive">
                            <table class="table table-striped text-center">
                            <thead>
                                <th> IdRazonSocial </th>
                                <th> IdEmpleado </th>
                                <th> NomAsig </th>
                                <th> NomProc </th>
                                <th> Año </th>
                                <th> Periodo </th>
                            </thead>
                            <tbody>`);                    
                response.data.forEach(element => {   
                    $('#tabColabs tbody:last').append(`
                    <tr>
                        <td>${element.IdRazonSocial}</td>
                        <td>${element.IdEmpleado}</td>
                        <td>${element.IdTipoNominaAsig}</td>
                        <td>${element.IdTipoNominaProc}</td>
                        <td>${element.Anio}</td>
                        <td>${element.Periodo}</td>
                    </tr>`
                    )
                });
            },
            error: function(xhr, error) {
                if (xhr.responseText) {
                    let error = xhr.responseText;
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong> Error: ${error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }else{
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }
            }
        });
    }
}

/**
 * @description Funcion para actualizar la fecha de pago del tipo de nómina
 * @returns {message: 'confirmation'} Retorna el mensaje de confirmacion o error
 */
 function postColabUpdate(){
    let url = "/postColabUpdate";
    let data = [];
    data = $('form').serializeArray();
    let verifyCheck = $('#checkColabs').is(":checked");
    let verify;
    if (verifyCheck) {
        verify = verifyColabsForm(data,1,'update'); //Con colaboradores
    }else{
        verify = verifyColabsForm(data,2,'update');
    }
    if (verify) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success:function(response,status) {
                if(response && status === 'success') {
                    if (response['data'] != 0 ) {
                        let dateUpdated = response['data'];
                        $('#actuallDate').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Los colaboradores han sido actualizados. Afectados: </strong>${dateUpdated}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    }else{
                        $('#actuallDate').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>!Error no se ha modificado ningún colaborador!!!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    }
                }
            },
            error: function(xhr, error) {
                if (xhr.responseText) {
                    let error = xhr.responseText;
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong> Error: ${error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }else{
                    $('#actuallDate').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }
            }
        });
    }
}

/**
 * @description Funcion para validar el formulario de colaboradores activos e inactivos.
 * @param {form} data 
 * @param { 1: con números de empleados , 2: sin numeros de empleados } type // se activa con el boton 'Aplica'
 * @param { 'consult': consultar datos, 'update': actualizar datos } typeRequest 
 * @returns true/false
 */
function verifyColabsForm(data,type,typeRequest){
    let razonsocialE = data[0].value;
    let nomAsigE = data[1].value;
    let nomProcE = data[2].value;
    let periodoE = data[3].value;
    let anioPeriodoE = data[4].value;
    let newIdAsig = data[5].value;
    let numEmpE = data[6].value;
    let radioAll = data[7].value;
    if (!razonsocialE) {
        $( "#razonsocialE" ).addClass( "is-invalid");    
    }else{
        $( "#razonsocialE" ).removeClass( "is-invalid");
        $( "#razonsocialE" ).addClass( "is-valid");
    }
    if (!nomAsigE) {
        $( "#nomAsigE" ).addClass( "is-invalid");    
    }else{
        $( "#nomAsigE" ).removeClass( "is-invalid");
        $( "#nomAsigE" ).addClass( "is-valid");
    }
    if (!anioPeriodoE) {
        $( "#anioPeriodoE" ).addClass( "is-invalid");    
    }else{
        $( "#anioPeriodoE" ).removeClass( "is-invalid");
        $( "#anioPeriodoE" ).addClass( "is-valid");
    }
    if (!periodoE) {
        $( "#periodoE" ).addClass( "is-invalid");    
    }else{
        $( "#periodoE" ).removeClass( "is-invalid");
        $( "#periodoE" ).addClass( "is-valid");
    }
    if (!numEmpE && type == 1) {
        $( "#numEmpE" ).addClass( "is-invalid");    
    }else if (numEmpE && type == 1){
        $( "#numEmpE" ).removeClass( "is-invalid");
        $( "#numEmpE" ).addClass( "is-valid");
    }else{
        $( "#numEmpE" ).removeClass( "is-invalid");
        $( "#numEmpE" ).removeClass( "is-valid");
        $( "#numEmpE" ).val(null);
    }
    if (!newIdAsig && (type == 1 || type == 2) && typeRequest === 'update') {
        $( "#newIdAsig" ).addClass( "is-invalid");
    }else if(newIdAsig && (type == 1 || type == 2) && typeRequest === 'update'){
        $( "#newIdAsig" ).removeClass( "is-invalid");
        $( "#newIdAsig" ).addClass( "is-valid");
    }
    if (numEmpE && type == 2){
        $( "#numEmpE" ).val(null);
        data[5].value = null;
    }
    if (!nomProcE) {
        $( "#nomProcE" ).addClass( "is-invalid");    
    }else{
        $( "#nomProcE" ).removeClass( "is-invalid");
        $( "#nomProcE" ).addClass( "is-valid");
    }
    if (razonsocialE && nomAsigE && anioPeriodoE && periodoE && nomProcE && type == 2 && typeRequest === 'consult') {
        return true;
    }else if(razonsocialE && nomAsigE && anioPeriodoE && periodoE && nomProcE && numEmpE && type == 1 && typeRequest === 'consult'){
        return true;
    }else if(razonsocialE && nomAsigE && anioPeriodoE && periodoE && nomProcE && type == 2 && newIdAsig && typeRequest === 'update'){
        return true;
    }else if(razonsocialE && nomAsigE && anioPeriodoE && periodoE && nomProcE && type == 1 && newIdAsig && typeRequest === 'update'){
        return true;
    }else{
        return false;
    }
}

/**
 * @description Funcion para limpiar el formulario de colaboradores activos e inactivos
 */
function clearFormColabs(){
    $( "#razonsocialE" ).removeClass( "is-invalid");
    $( "#razonsocialE" ).removeClass( "is-valid");
    $( "#nomAsigE" ).removeClass( "is-invalid");
    $( "#nomAsigE" ).removeClass( "is-valid");
    $( "#nomProcE" ).removeClass( "is-invalid");
    $( "#nomProcE" ).removeClass( "is-valid");
    $( "#anioPeriodoE" ).removeClass( "is-invalid");
    $( "#anioPeriodoE" ).removeClass( "is-valid");
    $( "#periodoE" ).removeClass( "is-invalid");
    $( "#periodoE" ).removeClass( "is-valid");
    $( "#fecActE" ).removeClass( "is-invalid");
    $( "#fecActE" ).removeClass( "is-valid");
    $( "#numEmpE" ).removeClass( "is-invalid");
    $( "#numEmpE" ).removeClass( "is-valid");
    $( "#newIdAsig" ).removeClass( "is-invalid");
    $( "#newIdAsig" ).removeClass( "is-valid");
    $( "#tabColabs" ).empty();
    $('form')[0].reset();
}

/**
 * @description Funcion para obtener los datos del periodo, si se encuentra abierto, cerrado o parcialmente cerrado.
 * @param { Tipo solicitud  } type 
 */
function inactPerdiodGet(type){
    var data = $('form').serializeArray();    
    let url = "/getDataMovtoMovtoHis";
    let verify = verifyFormPeriods(data,type);
    if (verify) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success:function(response,status) {
                if ( (response.movtohis.recordsets[0].length > 0 && status === 'success') || (response.movto.recordsets[0].length > 0 && status === 'success') ) {

                    // MovtoHis y movto cuentan con registros, el perdiodo no esta cerrado por completo
                    if(response.movtohis.recordsets[0].length > 0 && response.movto.recordsets[0].length > 0){
                        $('#dataSuccess').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>El periodo consultado se encuentra parcialmente cerrado. MovtoHis: ${response.movtohis.recordsets[0].length}, Movto: ${response.movto.recordsets[0].length}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    }
                    
                    // MovtoHis es mayor que movto, se encuentra cerrado el periodo
                    if (response.movtohis.recordsets[0].length > 0 && response.movto.recordsets[0].length == 0) {
                        $('#dataSuccess').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>El periodo consultado permanece cerrado. MovtoHis: ${response.movtohis.recordsets[0].length}, Movto: ${response.movto.recordsets[0].length}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);                    
                    }

                    // Movto es mayor que movtohis, se encuentra abierto el periodo
                    if(response.movtohis.recordsets[0].length == 0 && response.movto.recordsets[0].length > 0){
                        $('#dataSuccess').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>El periodo consultado permanece abierto. MovtoHis: ${response.movtohis.recordsets[0].length}, Movto: ${response.movto.recordsets[0].length}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    }

                // La informacion insertada no es correcta o no se encontraron registros
                }else if( response.movtohis.recordsets[0].length == 0 && response.movto.recordsets[0].length == 0 && status === 'success' ){
                    $('#dataSuccess').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <strong>No se han encontrado resultados. Verifíque la información.</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                }                
            },
            error: function(xhr, error) {
                if (xhr.responseText) {
                    let error = xhr.responseText;
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong> Error: ${error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }else{
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }
            }
        });
    }else{
        // console.log("faltan datos");
    }  
}

function consultPeriod(type){
    let url = "/getInactPerdiod";
    var data = $('form').serializeArray();
    let verify = verifyFormPeriods(data,type);
    if (verify) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success:function(response,status) {
                if(response.length > 0 && status === 'success') {
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="table-responsive">
                                <table class="table table-striped text-center">
                                <thead>
                                    <th>IdRazonSocial</th>
                                    <th>IdTipoNomina</th>
                                    <th>Periodo</th>
                                    <th>Año</th>
                                    <th>Estatus</th>
                                    <th>Fecha pago</th>
                                </thead>
                                <tbody>`);
                    $.each(response,function(key,value){
                        $('#dataSuccess tbody:last').append(`
                            <tr>
                                <td>${value['IdRazonSocial']}</td>
                                <td>${value['IdTipoNomina']}</td>
                                <td>${value['Periodo']}</td>
                                <td>${value['AnioPeriodo']}</td>
                                <td>${value['Status']}</td>
                                <td>${value['FechaPago']}</td>
                            </tr>`
                        );
                    });
                }else if(response.length == 0 && status === 'success'){
                    console.log("vacio");
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <strong>No se han encontrado resultados. Verifíque la información.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }
            },
            error: function(xhr, error) {
                if (xhr.responseText) {
                    let error = xhr.responseText;
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong> Error: ${error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }else{
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }
            }
        });
    }else{
        // console.log("faltan datos");
    }  
}

/**
     * @description Funcion que evalua sí el formulario de cambio de fecha ha sido llenado.
     * @param { datos_formulario } data 
     * @returns true/false 
 */
 function verifyFormPeriods(data,type){
    let razonsocialI = data[0].value;
    let nomAsigI = data[1].value;
    let nomProcI = data[2].value;
    let periodoI = data[3].value;
    let anioPeriodoI = data[4].value;
    if (type == 1 || type == 3) {
        if (!razonsocialI) {
            $( "#razonsocialI" ).addClass( "is-invalid");    
        }else{
            $( "#razonsocialI" ).removeClass( "is-invalid");
            $( "#razonsocialI" ).addClass( "is-valid");
        }
        if (!nomAsigI) {
            $( "#nomAsigI" ).addClass( "is-invalid");    
        }else{
            $( "#nomAsigI" ).removeClass( "is-invalid");
            $( "#nomAsigI" ).addClass( "is-valid");
        }
        if (!periodoI) {
            $( "#periodoI" ).addClass( "is-invalid");    
        }else{
            $( "#periodoI" ).removeClass( "is-invalid");
            $( "#periodoI" ).addClass( "is-valid");
        }
        if (!anioPeriodoI) {
            $( "#anioPeriodoI" ).addClass( "is-invalid");    
        }else{
            $( "#anioPeriodoI" ).removeClass( "is-invalid");
            $( "#anioPeriodoI" ).addClass( "is-valid");
        }
        if (!nomProcI) {
            $( "#nomProcI" ).addClass( "is-invalid");    
        }else{
            $( "#nomProcI" ).removeClass( "is-invalid");
            $( "#nomProcI" ).addClass( "is-valid");
        }        
    }    
    if (type == 2) {
        nomProcI = null;
        if (!razonsocialI) {
            $( "#razonsocialI" ).addClass( "is-invalid");    
        }else{
            $( "#razonsocialI" ).removeClass( "is-invalid");
            $( "#razonsocialI" ).addClass( "is-valid");
        }
        if (!nomAsigI) {
            $( "#nomAsigI" ).addClass( "is-invalid");    
        }else{
            $( "#nomAsigI" ).removeClass( "is-invalid");
            $( "#nomAsigI" ).addClass( "is-valid");
        }
        if (!periodoI) {
            $( "#periodoI" ).addClass( "is-invalid");    
        }else{
            $( "#periodoI" ).removeClass( "is-invalid");
            $( "#periodoI" ).addClass( "is-valid");
        }
        if (!anioPeriodoI) {
            $( "#anioPeriodoI" ).addClass( "is-invalid");    
        }else{
            $( "#anioPeriodoI" ).removeClass( "is-invalid");
            $( "#anioPeriodoI" ).addClass( "is-valid");
        }
        $( "#nomProcI" ).removeClass( "is-invalid");
        $( "#nomProcI" ).removeClass( "is-valid");
        $("#nomProcI").val("");    
    }  
    if (razonsocialI && nomProcI && nomAsigI && anioPeriodoI && periodoI && type == 1) {
        return true;
    }else if(razonsocialI && nomAsigI && anioPeriodoI && periodoI && type == 2 && !nomProcI){
        return true;
    }
    else{
        return false;
    }
}

/**
 * @description Función para abrir o cerrar los periodos
 * @param { Tipo de solicitud } type 
 * @param { Tipo de cambio abrir/cerrar periodo } openCloseType 
 */
function changeStatusPeriod(type,openCloseType){
    let url = "/updPerActiInacti";
    var data = $('form').serializeArray();
    let verify = verifyFormPeriods(data,type);
    if (verify) {
        console.log(verify,data,openCloseType);
        $.ajax({
            url:  `${url}/${openCloseType}`,
            type: "POST",
            data: data,
            success:function(response,status) {
                console.log(response);
                console.log(response.message);
                console.log(response.type);
                console.log(Object.keys(response).length);
                console.log(response.length);
                if ( Object.keys(response).length > 0 && status === 'success') {
                    if (response.type === 'success') $('#dataSuccess').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <strong>${response.message}.</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                    if (response.type === 'error') $('#dataSuccess').append(`
                        <div class="col-sm-12.col-md-12">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>${response.message}.</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>`);
                }
            },
            error: function(xhr, error) {
                if (xhr.responseText) {
                    let error = xhr.responseText;
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong> Error: ${error}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }else{
                    $('#dataSuccess').append(`
                    <div class="col-sm-12.col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>!Error del servidor!, no está respondiendo.</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>`);
                }
            }
        });
    }
}

/**
 * @description Funcion para limpiar el formulario de colaboradores activos e inactivos
 */
 function clearFormPeriods(){
    $( "#razonsocialI" ).removeClass( "is-invalid");
    $( "#razonsocialI" ).removeClass( "is-valid");
    $( "#nomAsigI" ).removeClass( "is-invalid");
    $( "#nomAsigI" ).removeClass( "is-valid");
    $( "#nomProcI" ).removeClass( "is-invalid");
    $( "#nomProcI" ).removeClass( "is-valid");
    $( "#anioPeriodoI" ).removeClass( "is-invalid");
    $( "#anioPeriodoI" ).removeClass( "is-valid");
    $( "#periodoI" ).removeClass( "is-invalid");
    $( "#periodoI" ).removeClass( "is-valid");
    $( "#statusCal" ).removeClass( "is-invalid");
    $( "#statusCal" ).removeClass( "is-valid");
    $( "#tabColabs" ).empty();
    $('form')[0].reset();
}